/*	
	1. Write a simple JavaScript function to join all elements of 	the following array into a string:
	socialMedia =["Facebook", "Instagram", "Twitter", "Tiktok" ]
*/

	let socialMedia = ["Facebook", "Instagram", "Twitter", "Tiktok"];

	function joinElements() {
		let socialMediaString = socialMedia.toString();
		console.log(socialMediaString);
	};

	joinElements();


/*
	2. Create a function that will add an Avenger at the beginning of the array below:
	avengers = ["Ironman", "Captain America", "Thor"]
*/

	let avengers = ["Ironman", "Captain America", "Thor"];

	function addAvenger(){
		avengers.unshift("Black Widow");
		console.log(avengers);
	};

	addAvenger();


/*
	3. Create an array of your friends' names. Create a function that will remove the last name in the array. Then, replace the deleted name with other two names.
*/

	let friendsList = [

		"Jessy",
		"Vea",
		"Rose",
		"Arlene",
		"Yayie",
		"Aries"
	];

	function replaceFriend() {
		friendsList.splice(5, 1, "Agatha", "Nikka");
		console.log(friendsList);
	};

	replaceFriend();